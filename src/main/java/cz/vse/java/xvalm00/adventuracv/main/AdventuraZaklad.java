package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.MapaHry;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AdventuraZaklad extends Application {

    private ImageView viewKarkulky = new ImageView(
            new Image(MapaHry.class.getResourceAsStream("/zdroje/karkulka.png"),
                    30, 40.0, false, true));

    private ImageView viewVlka = new ImageView(
            new Image(MapaHry.class.getResourceAsStream("/zdroje/vlk.png"),
                    70, 50.0, false, true));

    private ImageView viewSrdce = new ImageView(
            new Image(MapaHry.class.getResourceAsStream("/zdroje/srdce.png"),
                    20, 30.0, false, true));

    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String... args) {
        IHra hra = new Hra();
        if (args.length > 0) {
            String prepinac = args[0];
            if ("text".equals(prepinac)) {
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
            } else if ("graphic".equals(prepinac)) {
                launch(args);
            } else {
                throw new IllegalArgumentException("Zvoleny vstupni parametr " + prepinac + " neznam.");
            }
        } else {
            launch(args);
        }
    }

    @Override
    public void start(Stage primaryStage) {
        BorderPane borderPane = new BorderPane();

        IHra hra = new Hra();
        TextArea textArea = new TextArea();
        textArea.setText(hra.vratUvitani());
        textArea.setEditable(false);
        borderPane.setCenter(textArea);

        PanelVychodu panelVychodu = new PanelVychodu(hra.getHerniPlan());
        borderPane.setRight(panelVychodu.getListView());

        MapaHry mapaHry = new MapaHry(hra.getHerniPlan(), viewKarkulky);
        AnchorPane mapaHryAnchorPane = mapaHry.getAnchorPane();
        borderPane.setTop(mapaHryAnchorPane);

        pridejSrdceAVlka(mapaHryAnchorPane);

        PanelBatohu panelBatohu = new PanelBatohu(hra);
        borderPane.setLeft(panelBatohu.getComponentNode());

        TextField prikazovePole = new TextField();

        pripravSpodniBox(borderPane, prikazovePole);
        pripravPrikazovePole(hra, textArea, prikazovePole);

        MenuBar menuBar = pripravMenu();
        VBox menuAHraVBox = new VBox();
        menuAHraVBox.getChildren().addAll(menuBar, borderPane);

        Scene scene = new Scene(menuAHraVBox, 600.0, 500.0);
        animujPohyby(scene);
        animujPohyby2();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void animujPohyby2() {
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(viewVlka);
        translate.setDuration(Duration.millis(1000));
        translate.setCycleCount(TranslateTransition.INDEFINITE);
        translate.setByX(-150.0);
        translate.setByY(-150.0);
        translate.setAutoReverse(true);
        translate.play();

        RotateTransition rotate = new RotateTransition();
        rotate.setNode(viewVlka);
        rotate.setDuration(Duration.millis(1000));
        rotate.setCycleCount(TranslateTransition.INDEFINITE);
        rotate.setByAngle(360.0);
        // rotate.setAutoReverse(true);
        rotate.play();
        // scale, fade
    }

    private void animujPohyby(Scene scene) {
        double karkulkaRychlost = 300.0; // pixels per unit of this computer's clock.

        LongProperty lastUpdateTime = new SimpleLongProperty(0);
        DoubleProperty karkulkyPohybX = new SimpleDoubleProperty(0.0);
        DoubleProperty karkulkyPohybY = new SimpleDoubleProperty(0.0);

        AnimationTimer karkulkaAnimation = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                if (lastUpdateTime.get() > 0) {
                    double elapsedSeconds = (timestamp - lastUpdateTime.get()) /
                            1_000_000_000.0;

                    // karkulkyPohybX.get() je příliš vysoké číslo, resp. je to počet pixelů za jednotku něčeho.
                    // to něco je vnitřní frekvence počítače.
                    // proto to v předchozím příkazu dělíme jedním GIGA Hertzem (bez měrných jednotek) a vypadá to aspoň nějak.
                    double zmenaX = elapsedSeconds * karkulkyPohybX.get();
                    double souradniceX = viewKarkulky.getTranslateX();

                    double zmenaY = elapsedSeconds * karkulkyPohybY.get();
                    double souradniceY = viewKarkulky.getTranslateY();

                    viewKarkulky.setTranslateX(souradniceX + zmenaX);
                    viewKarkulky.setTranslateY(souradniceY + zmenaY);
                }
                lastUpdateTime.set(timestamp);
            }
        };
        karkulkaAnimation.start();

        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.A) {
                karkulkyPohybX.set(-karkulkaRychlost);
            }
            if (event.getCode() == KeyCode.D) {
                karkulkyPohybX.set(karkulkaRychlost);
            }
            if (event.getCode() == KeyCode.W) {
                karkulkyPohybY.set(-karkulkaRychlost);
            }
            if (event.getCode() == KeyCode.S) {
                karkulkyPohybY.set(karkulkaRychlost);
            }
        });

        scene.setOnKeyReleased(event -> {
            karkulkyPohybX.set(0.0);
            karkulkyPohybY.set(0.0);
        });
    }

    private void pridejSrdceAVlka(AnchorPane mapaHryAnchorPane) {
        mapaHryAnchorPane.getChildren().addAll(viewVlka, viewSrdce);

        AnchorPane.setLeftAnchor(viewVlka, 300.0);
        AnchorPane.setTopAnchor(viewVlka, 150.0);

        viewVlka.setBlendMode(BlendMode.COLOR_BURN);

        AnchorPane.setLeftAnchor(viewSrdce, 400.0);
        AnchorPane.setTopAnchor(viewSrdce, 50.0);
    }

    private void pripravSpodniBox(BorderPane borderPane, TextField prikazovePole) {
        Label prikazovePoleLabel = new Label("Zadej prikaz:");
        prikazovePoleLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16.0));

        HBox spodniHBox = new HBox();
        spodniHBox.setAlignment(Pos.CENTER);
        spodniHBox.getChildren().addAll(prikazovePoleLabel, prikazovePole);

        borderPane.setBottom(spodniHBox);
    }

    private void pripravPrikazovePole(IHra hra, TextArea textArea, TextField prikazovePole) {
        prikazovePole.setOnAction(event -> {
            String prikaz = prikazovePole.getText();
            String navratovaHodnotaHry = hra.zpracujPrikaz(prikaz);
            textArea.appendText('\n' + navratovaHodnotaHry + '\n');
            prikazovePole.clear();
        });
    }

    private MenuBar pripravMenu() {
        MenuBar menuBar = new MenuBar();
        Menu souborMenu = new Menu("Soubor");
        Menu napovedaMenu = new Menu("Nápověda");

        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/maliny.jpg")));

         MenuItem novaHraMenuItem = new MenuItem("Nová Hra", novaHraIkonka);
//        MenuItem novaHraMenuItem = new MenuItem("Nová Hra");
        MenuItem konecMenuItem = new MenuItem("Konec");
        MenuItem oAplikaciMenuItem = new MenuItem("O Aplikaci");
        MenuItem napovedaMenuItem = new MenuItem("Nápověda");

        novaHraMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();

        souborMenu.getItems().addAll(novaHraMenuItem, separatorMenuItem, konecMenuItem);
        napovedaMenu.getItems().addAll(oAplikaciMenuItem, napovedaMenuItem);

        oAplikaciMenuItem.setOnAction(event -> {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
//	... set title, content,...
            alert.setTitle("Java FX Adventura");
            alert.setHeaderText("Header Text - Java FX Adventura");
            alert.setContentText("Verze ZS 2021");
                    alert.showAndWait();
        });

        napovedaMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource
                            ("/zdroje/napovedaManual.html").toExternalForm());
            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });

        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
        return menuBar;
    }
}
