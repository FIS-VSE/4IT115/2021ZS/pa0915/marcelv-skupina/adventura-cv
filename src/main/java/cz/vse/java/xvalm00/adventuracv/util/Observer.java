package cz.vse.java.xvalm00.adventuracv.util;

public interface Observer {

    /**
     * Reakce na pozorovanou změnu
     */
    void update();

}
