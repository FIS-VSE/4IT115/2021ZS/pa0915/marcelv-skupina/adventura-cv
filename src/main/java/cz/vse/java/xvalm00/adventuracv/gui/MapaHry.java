package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class MapaHry implements Observer {

    private AnchorPane anchorPane = new AnchorPane();
    private final HerniPlan herniPlan;
    private ImageView viewKarkulky;

    public MapaHry(HerniPlan herniPlan, ImageView viewKarkulky) {
        this.herniPlan = herniPlan;
        this.viewKarkulky = viewKarkulky;

        init();
    }

    private void init() {
        herniPlan.registerObserver(this);

        nactiObrazekHry();
    }

    private void nactiObrazekHry() {
        ImageView viewMapy = new ImageView(new Image(MapaHry.class.getResourceAsStream("/zdroje/herniPlan.png"), 400.0, 250.0, false, true));

        anchorPane.getChildren().addAll(viewMapy, viewKarkulky);

        aktualizujPoziciTecky();
    }

    @Override
    public void update() {
        aktualizujPoziciTecky();
    }

    private void aktualizujPoziciTecky() {
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        AnchorPane.setLeftAnchor(viewKarkulky, aktualniProstor.getPosLeft());
        AnchorPane.setTopAnchor(viewKarkulky, aktualniProstor.getPosTop());
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }
}
